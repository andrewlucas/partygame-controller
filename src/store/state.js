import { defineStore } from 'pinia'

export const useStateStore = defineStore({
  id: 'game',
  state: () => ({
    profile: null,
    colors: null,
    room: null,
    controller: null,
    data: null,
    showConfig: false,
    reconnecting: false,
  }),
  actions: {
    update(state) {
      this.state = state
    }
  }
})